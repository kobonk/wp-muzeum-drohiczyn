<?php
/*
Plugin Name: Muzeum Diecezjalne
Description: Wtyczka rozszerzająca motyw Theme51822
Author: AMU Studio Łukasz Mitura
Author URI: http://amustudio.pl
Version: 1.0
*/

function muzeum_add_styles() {
	wp_register_style( 'muzeum-style', plugins_url( 'css/muzeum-styles.min.css', __FILE__ ), array(), '1.0' );
	wp_enqueue_style( 'muzeum-style' );
}
add_action( 'wp_print_styles', 'muzeum_add_styles' );
 

function muzeum_add_scripts() {
	wp_register_script( 'muzeum-script', plugins_url( 'js/muzeum-scripts.min.js', __FILE__ ), array(), '1.0' );
	wp_enqueue_script( 'muzeum-script' );
}
add_action( 'wp_print_scripts', 'muzeum_add_scripts' );


function muzeum_template_include( $template ) {
 
	if ( file_exists( untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/templates/' . basename( $template ) ) )
		$template = untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/templates/' . basename( $template );
 
	return $template;
}
add_filter( 'template_include', 'muzeum_template_include', 11 );

?>