<?php /* Wrapper Name: Header */ ?>
<div class="row">	
	<div class="span7" data-motopress-type="dynamic-sidebar" data-motopress-sidebar-id="header-sidebar">
		<?php dynamic_sidebar("header-sidebar"); ?>
	</div>
	<div class="span5">		
		<div class="hidden-phone" data-motopress-type="static" data-motopress-static-file="static/static-search.php">
			<?php get_template_part("static/static-search"); ?>
		</div>
		<!-- Social Links -->
		<div class="social-nets-wrapper" data-motopress-type="static" data-motopress-static-file="static/static-social-networks.php">
			<?php get_template_part("static/static-social-networks"); ?>
		</div>
		<!-- /Social Links -->
	</div>	
</div>
<div class="nav-wrapper">
	<div class="row">
		<div class="span3" data-motopress-type="static" data-motopress-static-file="static/static-logo.php">
			<?php get_template_part("static/static-logo"); ?>
		</div>
		<div class="span9" data-motopress-type="static" data-motopress-static-file="static/static-nav.php">
			<?php get_template_part("static/static-nav"); ?>
		</div>
	</div>
</div>