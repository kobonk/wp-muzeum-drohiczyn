<?php
	// Loads child theme textdomain
	load_child_theme_textdomain( CURRENT_THEME, CHILD_DIR . '/languages' );

	// Loads custom scripts.
	// require_once( 'custom-js.php' );

	add_action( 'wp_enqueue_scripts', 'cherry_child_custom_scripts' );
	function cherry_child_custom_scripts() {
		wp_enqueue_script( 'search-focus', CHILD_URL . '/js/search-focus.js', array( 'jquery' ), '1.0', true );
	}

	add_filter( 'cherry_stickmenu_selector', 'cherry_change_selector' );
	function cherry_change_selector($selector) {
		$selector = '.nav-wrapper';
		return $selector;
	}

	/**
	 * Banner
	 *
	 */
	if ( !function_exists( 'banner_shortcode' ) ) {

		function banner_shortcode( $atts, $content = null, $shortcodename = '' ) {
			extract( shortcode_atts(
				array(
					'img'          => '',
					'banner_link'  => '',
					'title'        => '',
					'text'         => '',
					'btn_text'     => '',
					'target'       => '',
					'custom_class' => ''
			), $atts));

			// Get the URL to the content area.
			$content_url = untrailingslashit( content_url() );

			// Find latest '/' in content URL.
			$last_slash_pos = strrpos( $content_url, '/' );

			// 'wp-content' or something else.
			$content_dir_name = substr( $content_url, $last_slash_pos - strlen( $content_url ) + 1 );

			$pos = strpos( $img, $content_dir_name );

			if ( false !== $pos ) {

				$img_new = substr( $img, $pos + strlen( $content_dir_name ), strlen( $img ) - $pos );
				$img     = $content_url . $img_new;

			}

			$output =  '<div class="banner-wrap '.$custom_class.'">';

			if ($banner_link != "") {
				$output .= '<a href="'. $banner_link .'" title="'. $title .'">';
			}

				if ($img !="") {
					$output .= '<figure class="featured-thumbnail">';
						$output .= '<img src="' . $img .'" title="'. $title .'" alt="" />';
					$output .= '</figure>';
				}
				$output .= '<div class="desc"><div class="desc-inner">';

					if ($title!="") {
						$output .= '<h5>';
						$output .= $title;
						$output .= '</h5>';
					}
					if ($text!="") {
						$output .= '<p>';
						$output .= $text;
						$output .= '</p>';
					}				

				$output .= '</div></div>';

			if ($banner_link != "") {	
				$output .= '</a>';
			}

			if ($btn_text!="") {
				$output .=  '<div class="link-align banner-btn"><a href="'.$banner_link.'" title="'.$btn_text.'" class="btn btn-link" target="'.$target.'">';
				$output .= $btn_text;
				$output .= '</a></div>';
			}

			$output .= '</div><!-- .banner-wrap (end) -->';

			$output = apply_filters( 'cherry_plugin_shortcode_output', $output, $atts, $shortcodename );

			return $output;
		}
		add_shortcode('banner', 'banner_shortcode');

	} 

	/**
	 * Post Cycle
	 *
	 */
	if (!function_exists('shortcode_post_cycle')) {

		function shortcode_post_cycle( $atts, $content = null, $shortcodename = '' ) {
			extract(shortcode_atts(array(
					'num'              => '5',
					'type'             => 'post',
					'meta'             => '',
					'effect'           => 'slide',
					'thumb'            => 'true',
					'thumb_width'      => '200',
					'thumb_height'     => '180',
					'more_text_single' => '',
					'category'         => '',
					'custom_category'  => '',
					'excerpt_count'    => '15',
					'pagination'       => 'true',
					'navigation'       => 'true',
					'custom_class'     => ''
			), $atts));

			$type_post         = $type;
			$slider_pagination = $pagination;
			$slider_navigation = $navigation;
			$random            = gener_random(10);
			$i                 = 0;
			$rand              = rand();
			$count             = 0;
			if ( is_rtl() ) {
				$is_rtl = 'true';
			} else {
				$is_rtl = 'false';
			}

			$output = '<script type="text/javascript">
							jQuery(window).load(function() {
								jQuery("#flexslider_'.$random.'").flexslider({
									animation: "'.$effect.'",
									smoothHeight : true,
									directionNav: '.$slider_navigation.',
									controlNav: '.$slider_pagination.',
									rtl: '.$is_rtl.',
									pauseOnHover: true
								});
							});';
			$output .= '</script>';
			$output .= '<div id="flexslider_'.$random.'" class="flexslider no-bg '.$custom_class.'">';
				$output .= '<ul class="slides">';

				global $post;
				global $my_string_limit_words;

				// WPML filter
				$suppress_filters = get_option('suppress_filters');

				$args = array(
					'post_type'              => $type_post,
					'category_name'          => $category,
					$type_post . '_category' => $custom_category,
					'numberposts'            => $num,
					'orderby'                => 'post_date',
					'order'                  => 'DESC',
					'suppress_filters'       => $suppress_filters
				);

				$latest = get_posts($args);

				foreach($latest as $key => $post) {
					//Check if WPML is activated
					if ( defined( 'ICL_SITEPRESS_VERSION' ) ) {
						global $sitepress;

						$post_lang = $sitepress->get_language_for_element($post->ID, 'post_' . $type_post);
						$curr_lang = $sitepress->get_current_language();
						// Unset not translated posts
						if ( $post_lang != $curr_lang ) {
							unset( $latest[$key] );
						}
						// Post ID is different in a second language Solution
						if ( function_exists( 'icl_object_id' ) ) {
							$post = get_post( icl_object_id( $post->ID, $type_post, true ) );
						}
					}
					setup_postdata($post);
					$excerpt        = get_the_excerpt();
					$attachment_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					$url            = $attachment_url['0'];
					$image          = aq_resize($url, $thumb_width, $thumb_height, true);

					$output .= '<li class="list-item-'.$count.'">';

						if ($thumb == 'true') {

							if ( has_post_thumbnail($post->ID) ){
								$output .= '<figure class="thumbnail featured-thumbnail"><a href="'.get_permalink($post->ID).'" title="'.get_the_title($post->ID).'">';
								$output .= '<img  src="'.$image.'" alt="'.get_the_title($post->ID).'" />';
								$output .= '</a></figure>';
							} else {

								$thumbid = 0;
								$thumbid = get_post_thumbnail_id($post->ID);

								$images = get_children( array(
									'orderby'        => 'menu_order',
									'order'          => 'ASC',
									'post_type'      => 'attachment',
									'post_parent'    => $post->ID,
									'post_mime_type' => 'image',
									'post_status'    => null,
									'numberposts'    => -1
								) );

								if ( $images ) {

									$k = 0;
									//looping through the images
									foreach ( $images as $attachment_id => $attachment ) {
										// $prettyType = "prettyPhoto-".$rand ."[gallery".$i."]";
										//if( $attachment->ID == $thumbid ) continue;

										$image_attributes = wp_get_attachment_image_src( $attachment_id, 'full' ); // returns an array
										$img = aq_resize( $image_attributes[0], $thumb_width, $thumb_height, true ); //resize & crop img
										$alt = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
										$image_title = $attachment->post_title;

										if ( $k == 0 ) {
											$output .= '<figure class="featured-thumbnail">';
											$output .= '<a href="'.get_permalink($post->ID).'" title="'.get_the_title($post->ID).'">';
											$output .= '<img  src="'.$img.'" alt="'.get_the_title($post->ID).'" />';
											$output .= '</a></figure>';
										} break;
										$k++;
									}
								}
							}
						}

						$output .= '<div class="desc"><div class="desc-inner">';

						$output .= '<h5><a href="'.get_permalink($post->ID).'" title="'.get_the_title($post->ID).'">';
						$output .= get_the_title($post->ID);
						$output .= '</a></h5>';

						if($meta == 'true'){
							$output .= '<span class="meta">';
							$output .= '<span class="post-date">';
							$output .= get_the_date();
							$output .= '</span>';
							$output .= '<span class="post-comments">'.__('Comments', CHERRY_PLUGIN_DOMAIN).": ";
							$output .= '<a href="'.get_comments_link($post->ID).'">';
							$output .= get_comments_number($post->ID);
							$output .= '</a>';
							$output .= '</span>';
							$output .= '</span>';
						}
						//display post options
						$output .= '<div class="post_options">';

						switch( $type_post ) {

							case "team":
								$teampos    = get_post_meta( $post->ID, 'my_team_pos', true );
								$team_email = sanitize_email( get_post_meta( $post->ID, 'my_team_email', true ) );
								$teaminfo   = get_post_meta( $post->ID, 'my_team_info', true );

								if ( !empty( $teampos ) ) {
									$output .= "<span class='page-desc'>" . $teampos . "</span><br>";
								}
								if ( !empty( $team_email ) && is_email( $team_email ) ) {
									$output .= '<span class="team-email"><a href="mailto:' . antispambot( $team_email, 1 ) . '">' . antispambot( $team_email ) . ' </a></span><br>';
								}
								if ( !empty( $teaminfo ) ) {
									$output .= '<span class="team-content post-content team-info">' . esc_html( $teaminfo ) . '</span>';
								}
								$output .= cherry_get_post_networks(array('post_id' => $post->ID, 'display_title' => false, 'output_type' => 'return'));
								break;

							case "testi":
								$testiname  = get_post_meta( $post->ID, 'my_testi_caption', true );
								$testiurl   = esc_url( get_post_meta( $post->ID, 'my_testi_url', true ) );
								$testiinfo  = get_post_meta( $post->ID, 'my_testi_info', true );
								$testiemail = sanitize_email( get_post_meta($post->ID, 'my_testi_email', true ) );

								if ( !empty( $testiname ) ) {
									$output .= '<span class="user">' . $testiname . '</span>, ';
								}
								if ( !empty( $testiinfo ) ) {
									$output .= '<span class="info">' . $testiinfo . '</span><br>';
								}
								if ( !empty( $testiurl ) ) {
									$output .= '<a class="testi-url" href="' . $testiurl . '" target="_blank">' . $testiurl . '</a><br>';
								}
								if ( !empty( $testiemail ) && is_email( $testiemail ) ) {
									$output .= '<a class="testi-email" href="mailto:' . antispambot( $testiemail, 1 ) . '">' . antispambot( $testiemail ) . ' </a>';
								}
								break;

							case "portfolio":
								$portfolioClient = (get_post_meta($post->ID, 'tz_portfolio_client', true)) ? get_post_meta($post->ID, 'tz_portfolio_client', true) : "";
								$portfolioDate = (get_post_meta($post->ID, 'tz_portfolio_date', true)) ? get_post_meta($post->ID, 'tz_portfolio_date', true) : "";
								$portfolioInfo = (get_post_meta($post->ID, 'tz_portfolio_info', true)) ? get_post_meta($post->ID, 'tz_portfolio_info', true) : "";
								$portfolioURL = (get_post_meta($post->ID, 'tz_portfolio_url', true)) ? get_post_meta($post->ID, 'tz_portfolio_url', true) : "";
								$output .="<strong class='portfolio-meta-key'>".__('Client', CHERRY_PLUGIN_DOMAIN).": </strong><span> ".$portfolioClient."</span><br>";
								$output .="<strong class='portfolio-meta-key'>".__('Date', CHERRY_PLUGIN_DOMAIN).": </strong><span> ".$portfolioDate."</span><br>";
								$output .="<strong class='portfolio-meta-key'>".__('Info', CHERRY_PLUGIN_DOMAIN).": </strong><span> ".$portfolioInfo."</span><br>";
								$output .="<a href='".$portfolioURL."'>".__('Launch Project', CHERRY_PLUGIN_DOMAIN)."</a><br>";
								break;

							default:
								$output .="";
						};
						$output .= '</div>';

						if($excerpt_count >= 1){
							$output .= '<p class="excerpt">';
							$output .= my_string_limit_words($excerpt,$excerpt_count);
							$output .= '</p>';
						}

						if($more_text_single!=""){
							$output .= '<a href="'.get_permalink($post->ID).'" class="btn btn-primary" title="'.get_the_title($post->ID).'">';
							$output .= $more_text_single;
							$output .= '</a>';
						}

						$output .= '</div></div>';

					$output .= '</li>';
					$count++;
				}
				wp_reset_postdata(); // restore the global $post variable
				$output .= '</ul>';
			$output .= '</div>';

			$output = apply_filters( 'cherry_plugin_shortcode_output', $output, $atts, $shortcodename );

			return $output;
		}
		add_shortcode('post_cycle', 'shortcode_post_cycle');

	}

	//Recent Posts
	if (!function_exists('shortcode_recent_posts')) {

		function shortcode_recent_posts( $atts, $content = null, $shortcodename = '' ) {
			extract(shortcode_atts(array(
					'type'             => 'post',
					'category'         => '',
					'custom_category'  => '',
					'tag'              => '',
					'post_format'      => 'standard',
					'num'              => '5',
					'meta'             => 'true',
					'thumb'            => 'true',
					'thumb_width'      => '120',
					'thumb_height'     => '120',
					'more_text_single' => '',
					'excerpt_count'    => '0',
					'custom_class'     => ''
			), $atts));

			$output = '<ul class="recent-posts '.$custom_class.' unstyled">';

			global $post;
			global $my_string_limit_words;
			$item_counter = 0;
			// WPML filter
			$suppress_filters = get_option('suppress_filters');

			if($post_format == 'standard') {

				$args = array(
							'post_type'         => $type,
							'category_name'     => $category,
							'tag'               => $tag,
							$type . '_category' => $custom_category,
							'numberposts'       => $num,
							'orderby'           => 'post_date',
							'order'             => 'DESC',
							'tax_query'         => array(
							'relation'          => 'AND',
								array(
									'taxonomy' => 'post_format',
									'field'    => 'slug',
									'terms'    => array('post-format-aside', 'post-format-gallery', 'post-format-link', 'post-format-image', 'post-format-quote', 'post-format-audio', 'post-format-video'),
									'operator' => 'NOT IN'
								)
							),
							'suppress_filters' => $suppress_filters
						);

			} else {

				$args = array(
					'post_type'         => $type,
					'category_name'     => $category,
					'tag'               => $tag,
					$type . '_category' => $custom_category,
					'numberposts'       => $num,
					'orderby'           => 'post_date',
					'order'             => 'DESC',
					'tax_query'         => array(
					'relation'          => 'AND',
						array(
							'taxonomy' => 'post_format',
							'field'    => 'slug',
							'terms'    => array('post-format-' . $post_format)
						)
					),
					'suppress_filters' => $suppress_filters
				);
			}

			$latest = get_posts($args);

			foreach($latest as $k => $post) {
					//Check if WPML is activated
					if ( defined( 'ICL_SITEPRESS_VERSION' ) ) {
						global $sitepress;

						$post_lang = $sitepress->get_language_for_element($post->ID, 'post_' . $type);
						$curr_lang = $sitepress->get_current_language();
						// Unset not translated posts
						if ( $post_lang != $curr_lang ) {
							unset( $latest[$k] );
						}
						// Post ID is different in a second language Solution
						if ( function_exists( 'icl_object_id' ) ) {
							$post = get_post( icl_object_id( $post->ID, $type, true ) );
						}
					}
					setup_postdata($post);
					$excerpt        = get_the_excerpt();
					$attachment_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					$url            = $attachment_url['0'];
					$image          = aq_resize($url, $thumb_width, $thumb_height, true);

					$post_classes = get_post_class();
					foreach ($post_classes as $key => $value) {
						$pos = strripos($value, 'tag-');
						if ($pos !== false) {
							unset($post_classes[$key]);
						}
					}
					$post_classes = implode(' ', $post_classes);

					$output .= '<li class="recent-posts_li ' . $post_classes . '  list-item-' . $item_counter . ' clearfix">';

					//Aside
					if($post_format == "aside") {

						$output .= the_content($post->ID);

					} elseif ($post_format == "link") {

						$url =  get_post_meta(get_the_ID(), 'tz_link_url', true);

						$output .= '<a target="_blank" href="'. $url . '">';
						$output .= get_the_title($post->ID);
						$output .= '</a>';

					//Quote
					} elseif ($post_format == "quote") {

						$quote =  get_post_meta(get_the_ID(), 'tz_quote', true);

						$output .= '<div class="quote-wrap clearfix">';

								$output .= '<blockquote>';
									$output .= $quote;
								$output .= '</blockquote>';

						$output .= '</div>';

					//Image
					} elseif ($post_format == "image") {

					if (has_post_thumbnail() ) :

						// $lightbox = get_post_meta(get_the_ID(), 'tz_image_lightbox', TRUE);

						$src      = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), array( '9999','9999' ), false, '' );

						$thumb    = get_post_thumbnail_id();
						$img_url  = wp_get_attachment_url( $thumb,'full'); //get img URL
						$image    = aq_resize( $img_url, 200, 120, true ); //resize & crop img


						$output .= '<figure class="thumbnail featured-thumbnail large">';
							$output .= '<a class="image-wrap" rel="prettyPhoto" title="' . get_the_title($post->ID) . '" href="' . $src[0] . '">';
							$output .= '<img src="' . $image . '" alt="' . get_the_title($post->ID) .'" />';
							$output .= '<span class="zoom-icon"></span></a>';
						$output .= '</figure>';

					endif;


					//Audio
					} elseif ($post_format == "audio") {

						$template_url = get_template_directory_uri();
						$id           = $post->ID;

						// get audio attribute
						$audio_title  = get_post_meta(get_the_ID(), 'tz_audio_title', true);
						$audio_artist = get_post_meta(get_the_ID(), 'tz_audio_artist', true);
						$audio_format = get_post_meta(get_the_ID(), 'tz_audio_format', true);
						$audio_url    = get_post_meta(get_the_ID(), 'tz_audio_url', true);

						// Get the URL to the content area.
						$content_url = untrailingslashit( content_url() );

						// Find latest '/' in content URL.
						$last_slash_pos = strrpos( $content_url, '/' );

						// 'wp-content' or something else.
						$content_dir_name = substr( $content_url, $last_slash_pos - strlen( $content_url ) + 1 );

						$pos = strpos( $audio_url, $content_dir_name );

						if ( false === $pos ) {
							$file = $audio_url;
						} else {
							$audio_new = substr( $audio_url, $pos + strlen( $content_dir_name ), strlen( $audio_url ) - $pos );
							$file     = $content_url . $audio_new;
						}

						$output .= '<script type="text/javascript">
							jQuery(document).ready(function(){
								var myPlaylist_'. $id.'  = new jPlayerPlaylist({
								jPlayer: "#jquery_jplayer_'. $id .'",
								cssSelectorAncestor: "#jp_container_'. $id .'"
								}, [
								{
									title:"'. $audio_title .'",
									artist:"'. $audio_artist .'",
									'. $audio_format .' : "'. stripslashes(htmlspecialchars_decode($file)) .'"}
								], {
									playlistOptions: {enableRemoveControls: false},
									ready: function () {jQuery(this).jPlayer("setMedia", {'. $audio_format .' : "'. stripslashes(htmlspecialchars_decode($file)) .'", poster: "'. $image .'"});
								},
								swfPath: "'. $template_url .'/flash",
								supplied: "'. $audio_format .', all",
								wmode:"window"
								});
							});
							</script>';

						$output .= '<div id="jquery_jplayer_'.$id.'" class="jp-jplayer"></div>
									<div id="jp_container_'.$id.'" class="jp-audio">
										<div class="jp-type-single">
											<div class="jp-gui">
												<div class="jp-interface">
													<div class="jp-progress">
														<div class="jp-seek-bar">
															<div class="jp-play-bar"></div>
														</div>
													</div>
													<div class="jp-duration"></div>
													<div class="jp-time-sep"></div>
													<div class="jp-current-time"></div>
													<div class="jp-controls-holder">
														<ul class="jp-controls">
															<li><a href="javascript:;" class="jp-previous" tabindex="1" title="'.__('Previous', CHERRY_PLUGIN_DOMAIN).'"><span>'.__('Previous', CHERRY_PLUGIN_DOMAIN).'</span></a></li>
															<li><a href="javascript:;" class="jp-play" tabindex="1" title="'.__('Play', CHERRY_PLUGIN_DOMAIN).'"><span>'.__('Play', CHERRY_PLUGIN_DOMAIN).'</span></a></li>
															<li><a href="javascript:;" class="jp-pause" tabindex="1" title="'.__('Pause', CHERRY_PLUGIN_DOMAIN).'"><span>'.__('Pause', CHERRY_PLUGIN_DOMAIN).'</span></a></li>
															<li><a href="javascript:;" class="jp-next" tabindex="1" title="'.__('Next', CHERRY_PLUGIN_DOMAIN).'"><span>'.__('Next', CHERRY_PLUGIN_DOMAIN).'</span></a></li>
															<li><a href="javascript:;" class="jp-stop" tabindex="1" title="'.__('Stop', CHERRY_PLUGIN_DOMAIN).'"><span>'.__('Stop', CHERRY_PLUGIN_DOMAIN).'</span></a></li>
														</ul>
														<div class="jp-volume-bar">
															<div class="jp-volume-bar-value"></div>
														</div>
														<ul class="jp-toggles">
															<li><a href="javascript:;" class="jp-mute" tabindex="1" title="'.__('Mute', CHERRY_PLUGIN_DOMAIN).'"><span>'.__('Mute', CHERRY_PLUGIN_DOMAIN).'</span></a></li>
															<li><a href="javascript:;" class="jp-unmute" tabindex="1" title="'.__('Unmute', CHERRY_PLUGIN_DOMAIN).'"><span>'.__('Unmute', CHERRY_PLUGIN_DOMAIN).'</span></a></li>
														</ul>
													</div>
												</div>
												<div class="jp-no-solution">
													<span>'.__('Update Required.', CHERRY_PLUGIN_DOMAIN).'</span>'.__('To play the media you will need to either update your browser to a recent version or update your ', CHERRY_PLUGIN_DOMAIN).'<a href="http://get.adobe.com/flashplayer/" target="_blank">'.__('Flash plugin', CHERRY_PLUGIN_DOMAIN).'</a>
												</div>
											</div>
										</div>
										<div class="jp-playlist">
											<ul>
												<li></li>
											</ul>
										</div>
									</div>';


					$output .= '<div class="entry-content">';
						$output .= get_the_content($post->ID);
					$output .= '</div>';

					//Video
					} elseif ($post_format == "video") {

						if ($thumb == 'true') {
							if ( has_post_thumbnail($post->ID) ){
								$output .= '<figure class="thumbnail featured-thumbnail"><a href="'.get_permalink($post->ID).'" title="'.get_the_title($post->ID).'">';
								$output .= '<img src="'.$image.'" alt="' . get_the_title($post->ID) .'"/>';
								$output .= '</a></figure>';
							}
						}

						if ($meta == 'true') {
							$output .= '<span class="meta">';
								$output .= '<span class="post-date">';
									$output .= get_the_date();
								$output .= '</span>';

								// post category
								$output .= '<span class="post-category">';
								if ($type!='' && $type!='post') {
									$terms = get_the_terms( $post_id, $type.'_category');
									if ( $terms && ! is_wp_error( $terms ) ) {
										$out = array();
										foreach ( $terms as $term )
											$out[] = '<a href="' .get_term_link($term->slug, $type.'_category') .'">'.$term->name.'</a>';
											$output .= join( ', ', $out );
									}
								} else {
									$categories = get_the_category($post_id);
									if($categories){
										$out = array();
										foreach($categories as $category)
											$out[] = '<a href="'.get_category_link($category->term_id ).'" title="'.$category->name.'">'.$category->cat_name.'</a> ';
											$output .= join( ', ', $out );
									}
								}
								$output .= '</span>';

								$output .= '<span class="post-comments">';
									$output .= '<a href="'.get_comments_link($post->ID).'">';
										$output .= get_comments_number($post->ID);
									$output .= '</a>';
								$output .= '</span>';
							$output .= '</span>';
						}

						$output .= '<h5><a href="'.get_permalink($post->ID).'" title="'.get_the_title($post->ID).'">';
								$output .= get_the_title($post->ID);
						$output .= '</a></h5>';

						if ($excerpt_count >= 1) {
							$output .= '<div class="excerpt">';
								$output .= my_string_limit_words($excerpt,$excerpt_count);
							$output .= '</div>';
						}

					//Standard
					} else {

						if ($thumb == 'true') {
							if ( has_post_thumbnail($post->ID) ){
								$output .= '<figure class="thumbnail featured-thumbnail"><a href="'.get_permalink($post->ID).'" title="'.get_the_title($post->ID).'">';
								$output .= '<img src="'.$image.'" alt="' . get_the_title($post->ID) .'"/>';
								$output .= '</a></figure>';
							}
						}

						if ($meta == 'true') {
							$output .= '<span class="meta">';
								$output .= '<span class="post-date">';
									$output .= get_the_date();
								$output .= '</span>';

								// post category
								$output .= '<span class="post-category">';
								if ($type!='' && $type!='post') {
									$terms = get_the_terms( $post_id, $type.'_category');
									if ( $terms && ! is_wp_error( $terms ) ) {
										$out = array();
										foreach ( $terms as $term )
											$out[] = '<a href="' .get_term_link($term->slug, $type.'_category') .'">'.$term->name.'</a>';
											$output .= join( ', ', $out );
									}
								} else {
									$categories = get_the_category($post_id);
									if($categories){
										$out = array();
										foreach($categories as $category)
											$out[] = '<a href="'.get_category_link($category->term_id ).'" title="'.$category->name.'">'.$category->cat_name.'</a> ';
											$output .= join( ', ', $out );
									}
								}
								$output .= '</span>';

								$output .= '<span class="post-comments">';
									$output .= '<a href="'.get_comments_link($post->ID).'">';
										$output .= get_comments_number($post->ID);
									$output .= '</a>';
								$output .= '</span>';
							$output .= '</span>';
						}	

						$output .= '<h5><a href="'.get_permalink($post->ID).'" title="'.get_the_title($post->ID).'">';
								$output .= get_the_title($post->ID);
						$output .= '</a></h5>';						

						$output .= cherry_get_post_networks(array('post_id' => $post->ID, 'display_title' => false, 'output_type' => 'return'));
						if ($excerpt_count >= 1) {
							$output .= '<div class="excerpt">';
								$output .= my_string_limit_words($excerpt,$excerpt_count);
							$output .= '</div>';
						}
						if ($more_text_single!="") {
							$output .= '<a href="'.get_permalink($post->ID).'" class="btn btn-primary" title="'.get_the_title($post->ID).'">';
							$output .= $more_text_single;
							$output .= '</a>';
						}
					}
				$output .= '<div class="clear"></div>';
				$item_counter ++;
				$output .= '</li><!-- .entry (end) -->';
			}
			wp_reset_postdata(); // restore the global $post variable
			$output .= '</ul><!-- .recent-posts (end) -->';

			$output = apply_filters( 'cherry_plugin_shortcode_output', $output, $atts, $shortcodename );

			return $output;
		}
		add_shortcode('recent_posts', 'shortcode_recent_posts');
	}

	/**
	 * Carousel Elastislide
	 */
	if ( !function_exists('shortcode_carousel') ) {
		function shortcode_carousel( $atts, $content = null, $shortcodename = '' ) {
			extract( shortcode_atts( array(
				'title'            => '',
				'num'              => 8,
				'type'             => 'post',
				'thumb'            => 'true',
				'thumb_width'      => 220,
				'thumb_height'     => 180,
				'more_text_single' => '',
				'category'         => '',
				'custom_category'  => '',
				'excerpt_count'    => 12,
				'date'             => '',
				'author'           => '',
				'comments'         => '',
				'min_items'        => 3,
				'spacer'           => 18,
				'custom_class'     => ''
			), $atts) );

			switch ( strtolower( str_replace(' ', '-', $type) ) ) {
				case 'blog':
					$type = 'post';
					break;
				case 'portfolio':
					$type = 'portfolio';
					break;
				case 'testimonial':
					$type = 'testi';
					break;
				case 'services':
					$type = 'services';
					break;
				case 'our-team':
					$type = 'team';
				break;
			}

			$carousel_uniqid = uniqid();
			$thumb_width     = absint( $thumb_width );
			$thumb_height    = absint( $thumb_height );
			$excerpt_count   = absint( $excerpt_count );
			$itemcount = 0;

			$output = '<div class="carousel-wrap ' . $custom_class . '">';
				if ( !empty( $title{0} ) ) {
					$output .= '<h2>' . esc_html( $title ) . '</h2>';
				}
				$output .= '<div id="carousel-' . $carousel_uniqid . '" class="es-carousel-wrapper">';
				$output .= '<div class="es-carousel">';
					$output .= '<ul class="es-carousel_list unstyled clearfix">';

						// WPML filter
						$suppress_filters = get_option( 'suppress_filters' );

						$args = array(
							'post_type'         => $type,
							'category_name'     => $category,
							$type . '_category' => $custom_category,
							'numberposts'       => $num,
							'orderby'           => 'post_date',
							'order'             => 'DESC',
							'suppress_filters'  => $suppress_filters
						);

						global $post; // very important
						$carousel_posts = get_posts( $args );

						foreach ( $carousel_posts as $key => $post ) {
							$post_id = $post->ID;

							//Check if WPML is activated
							if ( defined( 'ICL_SITEPRESS_VERSION' ) ) {
								global $sitepress;

								$post_lang = $sitepress->get_language_for_element( $post_id, 'post_' . $type );
								$curr_lang = $sitepress->get_current_language();
								// Unset not translated posts
								if ( $post_lang != $curr_lang ) {
									unset( $carousel_posts[$j] );
								}
								// Post ID is different in a second language Solution
								if ( function_exists( 'icl_object_id' ) ) {
									$post = get_post( icl_object_id( $post_id, $type, true ) );
								}
							}
							setup_postdata( $post ); // very important
							$post_title      = esc_html( get_the_title( $post_id ) );
							$post_title_attr = esc_attr( strip_tags( get_the_title( $post_id ) ) );
							$format          = get_post_format( $post_id );
							$format          = (empty( $format )) ? 'format-standart' : 'format-' . $format;
							if ( get_post_meta( $post_id, 'tz_link_url', true ) ) {
								$post_permalink = ( $format == 'format-link' ) ? esc_url( get_post_meta( $post_id, 'tz_link_url', true ) ) : get_permalink( $post_id );
							} else {
								$post_permalink = get_permalink( $post_id );
							}
							if ( has_excerpt( $post_id ) ) {
								$excerpt = wp_strip_all_tags( get_the_excerpt() );
							} else {
								$excerpt = wp_strip_all_tags( strip_shortcodes (get_the_content() ) );
							}

							$output .= '<li class="es-carousel_li ' . $format . ' clearfix list-item-'.$itemcount.'">';

								if ( $thumb == 'true' ) :

									if ( has_post_thumbnail( $post_id ) ) {
										$attachment_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'full' );
										$url            = $attachment_url['0'];
										$image          = aq_resize($url, $thumb_width, $thumb_height, true);

										$output .= '<figure class="featured-thumbnail">';
											$output .= '<a href="' . $post_permalink . '" title="' . $post_title . '">';
												$output .= '<img src="' . $image . '" alt="' . $post_title . '" />';
											$output .= '</a>';
										$output .= '</figure>';

									} else {

										$attachments = get_children( array(
											'orderby'        => 'menu_order',
											'order'          => 'ASC',
											'post_type'      => 'attachment',
											'post_parent'    => $post_id,
											'post_mime_type' => 'image',
											'post_status'    => null,
											'numberposts'    => 1
										) );
										if ( $attachments ) {
											foreach ( $attachments as $attachment_id => $attachment ) {
												$image_attributes = wp_get_attachment_image_src( $attachment_id, 'full' );
												$img              = aq_resize( $image_attributes[0], $thumb_width, $thumb_height, true );
												$alt              = get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true );

												$output .= '<figure class="featured-thumbnail">';
														$output .= '<a href="' . $post_permalink.'" title="' . $post_title . '">';
															$output .= '<img src="' . $img . '" alt="' . $alt . '" />';
													$output .= '</a>';
												$output .= '</figure>';
											}
										}
									}

								endif;

								$output .= '<div class="desc">';

									// post date
									if ( $date == 'yes' ) {
										$output .= '<time datetime="' . get_the_time( 'Y-m-d\TH:i:s', $post_id ) . '">' . get_the_date() . '</time>';
									}

									// post author
									if ( $author == 'yes' ) {
										$output .= '<em class="author">&nbsp;<span>' . __('by', CHERRY_PLUGIN_DOMAIN) . '</span>&nbsp;<a href="' . get_author_posts_url( get_the_author_meta( 'ID' ) ) . '">' . get_the_author_meta( 'display_name' ) . '</a> </em>';
									}

									// post comment count
									if ( $comments == 'yes' ) {
										$comment_count = $post->comment_count;
										if ( $comment_count >= 1 ) :
											$comment_count = $comment_count . ' <span>' . __( 'Comments', CHERRY_PLUGIN_DOMAIN ) . '</span>';
										else :
											$comment_count = $comment_count . ' <span>' . __( 'Comment', CHERRY_PLUGIN_DOMAIN ) . '</span>';
										endif;
										$output .= '<a href="'. $post_permalink . '#comments" class="comments_link">' . $comment_count . '</a>';
									}

									// post category
									$output .= '<span class="post-category">';
									if ($type!='' && $type!='post') {
										$terms = get_the_terms( $post_id, $type.'_category');
										if ( $terms && ! is_wp_error( $terms ) ) {
											$out = array();
											foreach ( $terms as $term )
												$out[] = '<a href="' .get_term_link($term->slug, $type.'_category') .'">'.$term->name.'</a>';
												$output .= join( ', ', $out );
										}
									} else {
										$categories = get_the_category($post_id);
										if($categories){
											$out = array();
											foreach($categories as $category)
												$out[] = '<a href="'.get_category_link($category->term_id ).'" title="'.$category->name.'">'.$category->cat_name.'</a> ';
												$output .= join( ', ', $out );
										}
									}
									$output .= '</span>';

									// post title
									if ( !empty($post_title{0}) ) {
										$output .= '<h5><a href="' . $post_permalink . '" title="' . $post_title_attr . '">';
											$output .= $post_title;
										$output .= '</a></h5>';
									}

									// post excerpt
									if ( !empty($excerpt{0}) ) {
										$output .= $excerpt_count > 0 ? '<p class="excerpt">' . my_string_limit_words( $excerpt, $excerpt_count ) . '</p>' : '';
									}

									// post more button
									$more_text_single = esc_html( wp_kses_data( $more_text_single ) );
									if ( $more_text_single != '' ) {
										$output .= '<a href="' . get_permalink( $post_id ) . '" class="btn btn-primary" title="' . $post_title_attr . '">';
											$output .= __( $more_text_single, CHERRY_PLUGIN_DOMAIN );
										$output .= '</a>';
									}
								$output .= '</div>';
							$output .= '</li>';
							$itemcount++;
						}
						wp_reset_postdata(); // restore the global $post variable

					$output .= '</ul>';
				$output .= '</div></div>';
				$output .= '<script>
					jQuery(document).ready(function(){
						jQuery("#carousel-' . $carousel_uniqid . '").elastislide({
							imageW  : ' . $thumb_width . ',
							minItems: ' . $min_items . ',
							speed   : 600,
							easing  : "easeOutQuart",
							margin  : ' . $spacer . ',
							border  : 0
						});
					})';
				$output .= '</script>';
			$output .= '</div>';

			$output = apply_filters( 'cherry_plugin_shortcode_output', $output, $atts, $shortcodename );

			return $output;
		}
		add_shortcode('carousel', 'shortcode_carousel');
	}

	//------------------------------------------------------
	//  Related Posts
	//------------------------------------------------------
	if(!function_exists('cherry_related_posts')){
		function cherry_related_posts($args = array()){
			global $post;
			$default = array(
				'post_type' => get_post_type($post),
				'class' => 'related-posts',
				'class_list' => 'related-posts_list',
				'class_list_item' => 'related-posts_item',
				'display_title' => true,
				'display_link' => true,
				'display_thumbnail' => true,
				'width_thumbnail' => 170,
				'height_thumbnail' => 170,
				'before_title' => '<h3 class="related-posts_h">',
				'after_title' => '</h3>',
				'posts_count' => 4
			);
			extract(array_merge($default, $args));

			$post_tags = wp_get_post_terms($post->ID, $post_type.'_tag', array("fields" => "slugs"));
			$tags_type = $post_type=='post' ? 'tag' : $post_type.'_tag' ;
			$suppress_filters = get_option('suppress_filters');// WPML filter
			$blog_related = apply_filters( 'cherry_text_translate', of_get_option('blog_related'), 'blog_related' );
			if ($post_tags && !is_wp_error($post_tags)) {
				$args = array(
					"$tags_type" => implode(',', $post_tags),
					'post_status' => 'publish',
					'posts_per_page' => $posts_count,
					'ignore_sticky_posts' => 1,
					'post__not_in' => array($post->ID),
					'post_type' => $post_type,
					'suppress_filters' => $suppress_filters
					);
				query_posts($args);
				if ( have_posts() ) {
					$output = '<div class="'.$class.'">';
					$output .= $display_title ? $before_title.$blog_related.$after_title : '' ;
					$output .= '<ul class="'.$class_list.' clearfix">';
					while( have_posts() ) {
						the_post();
						$thumb   = has_post_thumbnail() ? get_post_thumbnail_id() : PARENT_URL.'/images/empty_thumb.gif';
						$blank_img = stripos($thumb, 'empty_thumb.gif');
						$img_url = $blank_img ? $thumb : wp_get_attachment_url( $thumb,'full');
						$image   = $blank_img ? $thumb : aq_resize($img_url, $width_thumbnail, $height_thumbnail, true) or $img_url;

						$output .= '<li class="'.$class_list_item.'">';
						$output .= $display_thumbnail ? '<figure class="thumbnail featured-thumbnail"><a href="'.get_permalink().'" title="'.get_the_title().'"><img data-src="'.$image.'" alt="'.get_the_title().'" /></a></figure>': '' ;
						$output .= $display_link ? '<a href="'.get_permalink().'" >'.get_the_title().'</a>': '' ;
						$output .= '</li>';
					}
					$output .= '</ul></div>';
					echo $output;
				}
				wp_reset_query();
			}
		}
	}

	/*-----------------------------------------------------------------------------------*/
	/* Custom Comments Structure
	/*-----------------------------------------------------------------------------------*/
	if ( !function_exists( 'mytheme_comment' ) ) {
		function mytheme_comment($comment, $args, $depth) {
			$GLOBALS['comment'] = $comment;
		?>
		<li <?php comment_class('clearfix'); ?> id="li-comment-<?php comment_ID() ?>">
			<div id="comment-<?php comment_ID(); ?>" class="comment-body clearfix">
				<div class="wrapper">
					<div class="comment-author vcard">
						<?php echo get_avatar( $comment->comment_author_email, 80 ); ?>
						<?php printf('<span class="author"><i class="icon-user"></i>%1$s</span>', get_comment_author_link()) ?>
					</div>
					<?php if ($comment->comment_approved == '0') : ?>
						<em><?php echo theme_locals("your_comment") ?></em>
					<?php endif; ?>
					<div class="extra-wrap">
						<?php comment_text(); ?>

						<div class="extra-wrap">
							<div class="reply">
								<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
							</div>
							<div class="comment-meta commentmetadata"><i class="icon-calendar"></i><?php printf('%1$s', get_comment_date()) ?></div>
						</div>
					</div>
				</div>				
			</div>
	<?php }
	}

?>